from django.apps import AppConfig


class NewspaperAppConfig(AppConfig):
    name = 'Newspaper_App'
