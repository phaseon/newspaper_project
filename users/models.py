from django.contrib.auth.models import AbstractUser
from django.db import models

# Create your models here.


class CustomUser(AbstractUser):
    age = models.PositiveIntegerField(default=0)
    country = models.CharField(max_length=2)  # ISO Alpha-2 (Two letter country code)
    state = models.CharField(max_length=2)
    city = models.CharField(max_length=80)
