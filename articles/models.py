from django.conf import settings
from django.db import models
from django.urls import reverse
from django.utils import timezone


# Create your models here.


class Article(models.Model):
    title = models.CharField(max_length=255)
    body = models.TextField()
    author = models.ForeignKey(  # Get user from auth model and store as author
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )

    created = models.DateTimeField(editable=False)
    modified = models.DateTimeField(editable=False)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('article_detail', args=[str(self.id)])

    def save(self, *args, **kwargs):
        """
        Every time a record is saved or modified, create/mod these fields.
        """
        if not self.id:
            self.created = timezone.now()
        self.modified = timezone.now()
        return super(Article, self).save(*args, **kwargs)


class Comment(models.Model):
    """
    Comments have a many to one relationship with articles, we want to make sure that one delete
    of an article, the related comments are also deleted.
    """
    article = models.ForeignKey(Article, on_delete=models.CASCADE, related_name='comments')
    comment = models.CharField(max_length=140)
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE
    )

    created = models.DateTimeField(editable=False)
    modified = models.DateTimeField(editable=False, blank=True)

    def save(self, *args, **kwargs):
        """
        Every time a record is saved or modified, create/mod these fields.
        """
        if not self.id:
            self.created = timezone.now()
        self.modified = timezone.now()
        return super(Comment, self).save(*args, **kwargs)

    def __str__(self):
        return self.comment

    def get_absolute_url(self):
        return reverse('article_list', args=[str(self.article.pk)])
