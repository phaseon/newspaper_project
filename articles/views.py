from django.views.generic import ListView, DetailView, DeleteView, UpdateView, CreateView, FormView
from django.views.generic.edit import FormMixin
from django import forms
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy, reverse
from django.http import HttpResponseForbidden
from .forms import CommentForm
from .models import Article, Comment


# Create your views here.


class ArticleCreateView(LoginRequiredMixin, CreateView):
    model = Article
    template_name = 'article_new.html'
    fields = ['title', 'body', ]
    login_url = 'login'


class ArticleListView(ListView):
    model = Article
    template_name = 'article_list.html'
    login_url = 'login'


class ArticleDetailView(LoginRequiredMixin, DetailView, FormMixin):
    """
    Every DetailView will need a comment form,
    we use the FormMixin to render both.
    """
    model = Article
    form_class = CommentForm
    template_name = 'article_detail.html'
    login_url = 'login'

    def get_success_url(self):  # Upon success, go back to the article list.
        return reverse('article_list')

    def get_context_data(self, **kwargs):  # Render form in template
        context = super().get_context_data(**kwargs)
        context['form'] = CommentForm()
        return context
    """
    What to do on POST. We prevent unauthed users from posting.
    Even though we use the LoginReqd mixin, its better to be safe than sorry.
    """
    def post(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return HttpResponseForbidden()
        # Get current object (article) open in DetailView
        self.object = self.get_object()
        # Instantiate our form
        form = self.get_form()
        if form.is_valid():
            form.save()
            return self.form_valid(form)
        else:
            return self.form_invalid(form)


class ArticleUpdateView(LoginRequiredMixin, UpdateView):
    model = Article
    fields = ['title', 'body', ]
    template_name = 'article_edit.html'
    login_url = 'login'


class ArticleDeleteView(LoginRequiredMixin, DeleteView):
    model = Article
    template_name = 'article_delete.html'
    success_url = reverse_lazy('article_list')
    login_url = 'login'
