from django.urls import path
from . import views

urlpatterns = [
    path('api/articles/', views.ArticleAPIView.as_view()),
    path('api/articles/<int:pk>', views.ArticleDetailAPIView.as_view()),
    path('api/articles/comments/', views.CommentAPIView.as_view()),
    path('api/articles/comments/<int:pk>/', views.CommentDetailAPIView.as_view()),
]
