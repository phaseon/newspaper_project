from django.forms import ModelForm, Textarea
from .models import Comment


# Create form from Comment Model
class CommentForm(ModelForm):
    class Meta:
        model = Comment
        fields = ['comment', 'article', 'author']
        """
        We want a textarea widget for input, not a singular line
        """
        widgets = {'comment': Textarea()}
