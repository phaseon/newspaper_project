from rest_framework import serializers
from articles import models

"""
This is where we define serializers for the API portion of this program.
We're using ModelSerializer here while specifying fields to remove ambiguity.
Even though we're still specifying fields, at least we don't have to create
validators, etc on our own.
"""


class ArticleSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Article
        fields = (
            'id',
            'title',
            'body',
            'author',
            'created',
            'modified',
        )


class CommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Comment
        fields = (
            'id',
            'article',
            'comment',
            'author',
            'created',
            'modified',
        )